const logger = require('./components/logger.js');
const data = require('./components/data.js');
const fs = require('fs');


module.exports.performBoottrap = performBoottrap;

function performBoottrap(callback, loopCount = 0) {
    data.getClientConfigItem('global', 'db_version', function(version){
        checkDbVersion(version, callback, loopCount);
    });  
};

function checkDbVersion(version, callback, loopCount = 0){
    if (loopCount < 6){
        if (!version){
            logger.info("Database NOT found");         
        }
        else{
            logger.info("Database found (V" + version + ")");
        }
        readFilesAndUpgrade(version, loopCount, function(){
            logger.debug("Database upgrade check complete");
            callback();
        });
    }
    else{
        logger.error("Timeout: Cannot upgrade database more than 5 versions");
    }
}

function readFilesAndUpgrade(version, loopCount = 0, callback){
    loopCount++;
    fs.readdir('./datascript/', (err, files) => { 
        var fileCount = files.length;
        files.sort().every(function(file, index){
            var ver = require(`./datascript/${file}`);
            //console.log(file);
            if (version < ver.version){
                logger.info("upgrading database " + version + " --> " + ver.version);
                (async () => await data.runDbCommandsAsync(ver.data(), function(){
                    logger.debug("Upgrade to v" + ver.version + " completed");
                    upgraded = true;
                    performBoottrap(callback, loopCount);    
                }))();   
            }
            else{
                logger.debug("Database already at v" + ver.version + " checking next");
                if (fileCount == (index + 1)){
                    callback();
                }
                return true;
            }
        });        
    });
}

