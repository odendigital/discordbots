require('dotenv').config();
const version = require('./version.json');
const boot = require('./bootstrap.js');
const Discord = require('discord.js');
const client = new Discord.Client();
const logger = require('./components/logger.js');
const fs = require('fs');

//TODO:
// record number of dsimisals per user, more than 5, warning
//1 - change user nickname
//2 - warn - set role, change nickname
//3 - add random reactions

// Setup development
logger.info("Running in V" + version.Version + " " + process.env.NODE_ENV);
if (process.env.NODE_ENV !== 'production') {    
    //development
    
}
else{    
}

logger.info("****************** Start ******************");

boot.performBoottrap(function(){
    client.login(process.env.DISCORD_TOKEN).then(postLogin);
});

fs.readdir('./events/', (err, files) => {
    files.forEach(file => {
        const eventHandler = require(`./events/${file}`)
        const eventName = file.split('.')[0]
        client.on(eventName, (...args) => eventHandler(client, ...args))
    })
})

function postLogin(){
}