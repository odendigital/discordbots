const admin = require('../components/admin.js');
const logger = require('../components/logger.js');
const data = require('../components/data.js');
const help = require('../commands/help.js');

module.exports = {
	name: 'add-random-nick',
	description: 'Adds a random nick to the pool of nick names!',
	execute(client, message, args) {
        admin.checkMessageFromAdmin(message, function(){
            addRandomNick(message, args["nick"], args['nsfw'], args['tags']);
        });
	},
};

function addRandomNick(message, nick, is_nsfw, tags) {
    logger.debug('Adding new nick ' + nick + ' for server ' + message.channel.guild.id);
    if (!nick){
        message.reply("Invalid paramaters, please check help");
        help.execute(null, message, { param : "add-random-nick" });
    }
    else{
        if (is_nsfw === undefined || is_nsfw === null){
            logger.debug("is_nsfw was empty, assuming true");
            is_nsfw = true;
        }
        else if (is_nsfw.trim() == "1"){
            is_nsfw = true;
        }    
        else if (is_nsfw.trim() == "0"){
            is_nsfw = false;
        }    
        data.addRandomNick(nick, tags, is_nsfw, message.channel.guild.id, function(result, error){
            if (!error){
                result = "ok";
                message.reply(result);
            }
            else{
                message.reply(error);
            }
        });
    }    
};