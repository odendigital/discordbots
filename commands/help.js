const discord = require('discord.js');
const logger = require('../components/logger.js');
const admin = require('../components/admin.js');

module.exports = {
	name: 'help',
	description: 'Displays help message!',
	execute(client, message, args) {
        admin.checkMessageFromAdmin(message, function(){
            if (args){
                helpCommand(message, args['param']);
            }
            else{
                help(message);
            }
        });		
	},
};

function help(message){
    var msg = new discord.RichEmbed()
        .setColor("#0049B0")
        .setTitle('Bot help')
        .setDescription('The following lsist some bot commands for admins to use. \n\
                In addtion the bot will listen for messages and react on certain key words or when certain words are exhanged and a user was mentioned.\n')
        .addField('!ping', "Test server comms")
        .addField('!ping', "Test server comms")
        .addField('!info', "Shows bot status info")
        .addField('!set-notice-channel', "Sets the channel used to post public notices (like warning notifications)")
        .addField('!set-warning-role', "Sets the role id for each warning level (1, 2, 3)")
        .addField('!add-random-nick', "Adds a nickname to the pool of random ones")
        .addField('!post-gif', "Posts a random gif to the notice channel")
        .addField('!set-user-nick', "Changes the users nick name to a random one from the pool")
        .addField('!warn-user', "Warns a user and sets his warning role")
        .addField('!repeat', "Posts the message to the notice channel as the bot")
        .addBlankField()
        .addField('To get help on specific command type ', '!help <COMMAND_NAME>');
    message.reply(msg); 
}

function helpCommand(message, command){
    var msg = new discord.RichEmbed().setColor("#0049B0");
    logger.debug("Command help with " + command);
    var send = false;
    if (command.startsWith("!")){
        command = command.substring(1);
    }
    switch(command.toLowerCase().trim()) {
    
        case "ping":
            send = true;
            msg.setTitle("Help on the !ping command")
            msg.addField('Usage', "!ping")
            break;

        case "info":
            send = true;
            msg.setTitle("Help on the !info command")
            msg.addField('Usage', "!info")
            break;

        case "set-notice-channel":
            send = true;
            msg.setTitle("Help on the !set-notice-channel command")
            msg.addField('Description', "Sets the channel the bot will post public messages and shamings to. Replace <CHANNEL_ID> with the appropriate channel id")
            msg.addField('Usage', "!set-notice-channel <CHANNEL_ID>")
            break;

        case "set-warning-role":
            send = true;
            msg.setTitle("Help on the !set-warning-role command")
            msg.addField('Description', "Sets the Role id for the warning level. Replace <WARNING-NO> with warning number (1,2,3) and <ROLEID> with the corresponding role id")
            msg.addField('Usage', "!set-warning-role warning:<WARNING-NO>, role:<ROLEID>")
            break;

        case "add-random-nick":
            send = true;
            msg.setTitle("Help on the !add-random-nick")
            msg.addField('Description', "Adds a nickname to the pool of random ones. Replace <NICK_NAME> with the nick name, <INDICATE_NSFW> with a 1 if NSFW or 0 if not, and (Optional) <TAGS> with tags to categorize nick name")
            msg.addField('Usage', "!add-random-nick nick:<NICK_NAME>, nsfw:<INDICATE_NSFW>, tags:<TAGS>")
            break;

        case "set-user-nick":
            send = true;
            msg.setTitle("Help on the !set-user-nick")
            msg.addField('Description', "Sets a random nick name to the user. Replace @<USER_NAME> with a user mention (@User)")
            msg.addField('Usage', "!set-user-nick @<USER_NAME>")
            break;
        
        case "post-gif":
            send = true;
            msg.setTitle("Help on the !post-gif")
            msg.addField('Description', "Posts a random gif to the channel. Replace <SEARCH_PHRASE> a phrase to search, \
                        (optional) replace <CHANNEL_ID> with a channel id to post to, omitting this will post to the set public channel, \
                        and <OPTIONAL_MESSAGE> with an optional message")
            msg.addField('Usage', "!post-gif gif:<SEARCH_PHRASE>, channel:<CHANNEL_ID>, msg:<OPTIONAL_MESSAGE>")
            break;

        case "warn-user":
            send = true;
            msg.setTitle("Help on the !warn-user")
            msg.addField('Description', "Warns the user. This sets his warning level (starting at 1) and assigns the warning role \
                        and posts a notice to the server notice channel. Additionaly if it is the first warning, the user will get a random nickname. Replace <REASON> with a reason for the warning")
            msg.addField('Usage', "!warn-user <REASON>")
            break;

        case "repeat":
            send = true;
            msg.setTitle("Help on the !repeat")
            msg.addField('Description', "Posts the message to the notice channel as the bot")
            msg.addField('Usage', "!repeat <YOUR_MESSAGE>")
            break;
        
        default:
            send = false;
            message.reply("Unable to help with " + command); 
    }
    if (send){
         message.reply(msg);    
    }  
}