const discord = require('discord.js');
const logger = require('../components/logger.js');
const admin = require('../components/admin.js');
const data = require('../components/data.js');
var version = require('../version.json');

module.exports = {
	name: 'info',
	description: 'Displays bot information!',
	execute(client, message, args) {
        admin.checkMessageFromAdmin(message, function(){
            info(message);
        });
	},
};

function info(message) {
    data.getClientConfig(message.channel.guild.id, function(result){
        var msg = new discord.RichEmbed()
            .setColor("#0049B0")
            .setTitle('Config')
            .addField('Version', version.Version)
            .addField('Env', process.env.NODE_ENV);
        if (result){
            var i;
            for (i = 0; i < result.length; i++) {
                msg.addField(result[i]["option"], result[i]["value"]);
            }
        }        
        message.reply(msg);
    });  
};