const admin = require('../components/admin.js');
const logger = require('../components/logger.js');
const data = require('../components/data.js');
const giphy = require('../components/giphy.js');
const messaging = require('../components/messaging.js')
const discord = require('discord.js');

module.exports = {
	name: 'post-gif',
	description: 'Posts a gif based on the search string!',
	execute(client, message, args) {
        admin.checkMessageFromAdmin(message, function(){
            postGif(client, message, args["channel"], args["gif"], args["msg"])
        });
	},
};

function postGif(client, message, channel, searchString, msg) {
    //console.log(channel);
    //console.log(client.guild);
    logger.debug('posting random gif of ' + searchString);
    if (!channel){
        data.getClientConfigItem(message.channel.guild.id, "notice_channel", function(data){
            if (data){
                giphy.random("'" + searchString + "'", function(res, err){
                    if (res){
                        //client.channels.get(data).send(res.data.image_url);
                        messaging.postMessage(res.data.image_url, msg, client, data);
                    }
                }); 
            }
            else{
                message.reply("No notice channel found");
            }            
        });
    }
    else{
        giphy.random(searchString, function(res, err){
            if (res){
                //client.channels.get(channel).send(res.data.image_url);
                messaging.postMessage(res.data.image_url, msg, client, channel);
            }
        });
    }   
};

