const admin = require('../components/admin.js');
const logger = require('../components/logger.js');
const data = require('../components/data.js');

module.exports = {
	name: 'repeat',
	description: 'Repeats the message in the notice channel!',
	execute(client, message, args) {
        admin.checkMessageFromAdmin(message, function(){
            repeatMessage(client, message, args["param"]);
        });
	},
};

function repeatMessage(client, message, param, channel) {
    data.getClientConfigItem(message.channel.guild.id, "notice_channel", function(result, error){
        if (result && result != null){
            //console.log(result);
            client.channels.get(result).send(param);
        }
        else{
            message.reply("Could not repeat message (" + error + ")");
        }        
    });
};