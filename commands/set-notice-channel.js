const admin = require('../components/admin.js');
const logger = require('../components/logger.js');
const data = require('../components/data.js');

module.exports = {
	name: 'set-notice-channel',
	description: 'Sets the notice channel for this server!',
	execute(client, message, args) {
        admin.checkMessageFromAdmin(message, function(){
            setNoticeChannel(message, args["param"]);
        });
	},
};

function setNoticeChannel(message, channel) {
    logger.debug('Setting ' + message.channel.guild.id + ' notice channel to ' + channel);
    data.setClientConfigItem(message.channel.guild.id, "notice_channel", channel, function(result, error){
        if (!error){
            result = "ok";
        }
        message.reply(result);
    });
};