const admin = require('../components/admin.js');
const logger = require('../components/logger.js');
const data = require('../components/data.js');
const messaging = require('../components/messaging.js')

module.exports = {
	name: 'set-user-nick',
	description: 'Gives the mentioned user a random nick name!',
	execute(client, message, args, callback) {
        admin.checkMessageFromAdmin(message, function(){
            //console.log(args);
            setUserNickName(message, client, args["msg"], callback);
        });
	},
};

function setUserNickName(message, client, msg, callback){
    logger.debug("setting user nick name");
    //console.log(msg);
    if (message.mentions.users.size > 0){
        message.mentions.users.forEach(user => {
            data.getRandomNick(message.channel.guild.id, function(nickResponse){
                logger.debug("Changing " + user.username + " to '" + nickResponse.nick_name + "'");
                message.guild.members.get(user.id).setNickname(nickResponse.nick_name);
                if (!msg){
                    msg = "<@" + user.id + "> you will now be know as " + nickResponse.nick_name;
                }
                else{
                    msg = "<@" + user.id + "> " + msg + ", you will now be know as " + nickResponse.nick_name;
                }
                if (!callback){
                    messaging.postMessage(null, msg, client, message);
                }                    
                else{
                    callback(nickResponse.nick_name);
                }
            });
        });        
    } 
    else{
        message.reply("There are no users in the message...WTF?!?");      
    }
}