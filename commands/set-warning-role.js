const admin = require('../components/admin.js');
const logger = require('../components/logger.js');
const data = require('../components/data.js');
const help = require('../commands/help.js');

module.exports = {
	name: 'set-warning-role',
	description: 'Sets the warning channel for this server!',
	execute(client, message, args) {
        admin.checkMessageFromAdmin(message, function(){
            setWarningRole(message, args["warning"], args['role']);
        });
	},
};

function setWarningRole(message, warning, role) {
    logger.debug('Setting warning ' + warning + ' role to ' + role);
    if (!warning || !role){
        message.reply("Invalid paramaters, please check help");
        help.execute(null, message, { param : "set-warning-role" });
    }
    else{
        data.setClientConfigItem(message.channel.guild.id, "warning" + warning, role, function(result, error){
            if (!error){
                result = "ok";
            }
            message.reply(result);
        });
    }    
};