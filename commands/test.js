const cache = require('../components/cache.js')
const logger = require('../components/logger.js');
const admin = require('../components/admin.js');

module.exports = {
	name: 'test',
	description: 'TEST',
	execute(client, message, args) {
		if (process.env.NODE_ENV == "development"){
			admin.checkMessageFromAdmin(message, function(){
				test(client, message, args);
			});	
		}
		else{
			logger.error("Cannot run test in production");
		}   	
	},
};

function test(client, message, args){
	var test = require('./warn-user.js');
	test.execute(client, message, args);
	// cache.getRandomGif("warning", function(gif){
	// 	console.log(gif);
	// });
};