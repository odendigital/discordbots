const admin = require('../components/admin.js');
const logger = require('../components/logger.js');
const data = require('../components/data.js');
const messaging = require('../components/messaging.js')
const setUserNick = require('./set-user-nick.js');
const cache = require('../components/cache.js')
const help = require('./help.js');

module.exports = {
	name: 'warn-user',
	description: 'Warns the user',
	execute(client, message, args) {
        admin.checkMessageFromAdmin(message, function(){
            warnUser(message, client, args["param"]);
        });
	},
};

function warnUser(message, client, msg){
    logger.debug("setting user warning");
    if (message.mentions.users.size > 0){
        data.getWarningRoles(message.channel.guild.id, function(roles){
            message.mentions.users.forEach(user => {
                //console.log(user.id);
                var guildMemeber = message.channel.guild.members.get(user.id.toString());
                roles.every(function(item, index){
                    var userRole = guildMemeber.roles.get(item.value);
                    if (userRole){
                        logger.debug(guildMemeber.displayName + " already has role " + item.option);
                        //has this role
                        if (index == roles.length - 1){
                            //last role, now what?
                            logger.debug("User has all the roles");
                            message.reply("User already at maximum warnings!");
                        }
                        return true;
                    }
                    else{
                        logger.debug(guildMemeber.displayName + " does NOT have role " + item.option);
                        assignUserRole(client, message, guildMemeber, item.value, item.option, msg);
                        return false;
                    }                    
                });
            });
        });
    } 
    else{
        message.reply("There are no users in the message...WTF?!?");
    }
}

//https://discord.js.org/#/docs/main/stable/class/GuildMember?scrollTo=addRole
function assignUserRole(client, message, guildMemeber, roleId, roleName, msg){
    logger.debug("setting user (" + guildMemeber.displayName + ") role to " + roleName + " (" + roleId + ")");
    guildMemeber.addRole(roleId)
    .then(function(member){
        var warnMsg =  "<@" + guildMemeber.id + "> you have recieved a warning for " + msg;
        if (roleName == "warning1"){
            logger.debug("1st warning, changing nick name");
            setUserNick.execute(client, message, [], function(nickName){
                warnMsg =  warnMsg += ", you will now be know as " + nickName;
                responseAfterRole(warnMsg, client, message);
            });
        }
        else{
            responseAfterRole(warnMsg, client, message);
        }
    })
    .catch(function(err){
       logger.error("Unable to assign role " + err.toString()) ;
    });
}

function responseAfterRole(msg, client, message){
    cache.getRandomGif("warning", function(gif){
        messaging.postMessage(gif.url, msg, client, message);
    });   
}