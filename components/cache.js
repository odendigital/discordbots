const data = require('../components/data.js');
const logger = require('../components/logger.js');

module.exports.getReactWords = getReactWords;
module.exports.getInsults = getInsults;
module.exports.getRandomGif = getRandomGif;
module.exports.getRandomBotResponse = getRandomBotResponse;

var gifs, botResponses, keyWords, nicks;

function getRandomBotResponse(type, callback){
    getResponses(function(){
        if (botResponses[type]){
            var item = botResponses[type][ Math.floor( Math.random() * botResponses[type].length ) ];
            callback(item);
        }
        else{
            logger.error("Could not find any gifs of type " + type);
        }
    });
}

function getResponses(callback, loopCount = 0){
    if (!botResponses){
        logger.debug("BotResponses was empty");
        if (loopCount < 1){
            data.getBotResponses(function(returnData){                
                botResponses = buildCollection(botResponses, returnData);                
                getResponses(callback, loopCount + 1);
            });
        }        
        else{
            logger.error("Time out trying to get BotResponses");
            callback(botResponses);
        }
    }
    else{
        logger.debug("BotResponses was NOT empty");
        callback(botResponses);
    }
}

function getRandomGif(tp, callback){
    getGifs(function(){
        if (gifs[tp]){
            var item = gifs[tp][ Math.floor( Math.random() * gifs[tp].length ) ];
            callback(item);
        }
        else{
            logger.error("Could not find any gifs of type " + tp);
        }
    });
}

function getGifs(callback, loopCount = 0){
    if (!gifs){
        logger.debug("Gifs was empty");
        if (loopCount < 1){
            data.getGifs(function(returnData){                
                gifs = buildCollection(gifs, returnData);
                // returnData.forEach(gif => {
                //     console.log(gif.type);
                //     var typ = gif.type;
                //     if (!gifs){
                //         gifs = {
                //             typ : [ gif ]
                //         }
                //     } 
                //     else if (gifs[gif.type]){
                //         gifs[gif.type].push(gif);
                //     }
                //     else{
                //         gifs[gif.type] = [ gif ];
                //     }
                // });
                // console.log(gifs);
                //gifs = returnData;
                //gifs = new Map(data.map(obj => [obj.type, obj.value]));                
                getGifs(callback, loopCount + 1);
            });
        }        
        else{
            logger.error("Time out trying to get gifs");
            callback(gifs);
        }
    }
    else{
        logger.debug("Gifs was NOT empty");
        callback(gifs);
    }
}

function getReactWords(callback, loopCount = 0){
    if (!keyWords || !keyWords["react"]){
        logger.debug("Reactions was empty");
        if (loopCount < 1){
            data.getKeyWords("react", function(returnData){
                //console.log(returnData);
                //insults = returnData;
                keyWords = buildCollection(keyWords, returnData);
                //console.log(keyWords);
                getReactWords(callback, loopCount + 1);       
            });
        }        
        else{
            callback(keyWords["react"]);
        }
    }
    else{
        logger.debug("Reactions was NOT empty");
        callback(keyWords["react"]);
    }
}

function getInsults(callback, loopCount = 0){
    if (!keyWords || !keyWords["insult"]){
        logger.debug("Insults was empty");
        if (loopCount < 1){
            data.getKeyWords("insult", function(returnData){
                //console.log(returnData);
                //insults = returnData;
                keyWords = buildCollection(keyWords, returnData);
                //console.log(keyWords);
                getInsults(callback, loopCount + 1);       
            });
        }        
        else{
            callback(keyWords["insult"]);
        }
    }
    else{
        logger.debug("Insults was NOT empty");
        callback(keyWords["insult"]);
    }
}

function buildCollection(baseCollection, dbData){
    dbData.forEach(item => {                    
        //console.log(response.type);
        var itemType = item.type;
        if (!baseCollection){
            baseCollection = {};
            baseCollection[itemType] = [ item ];
        } 
        else if (baseCollection[itemType]){
            baseCollection[itemType].push(item);
        }
        else{
            baseCollection[itemType] = [ item ];
        }
    });
    return baseCollection;
}

