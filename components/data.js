const logger = require('./logger.js');
const MongoClient = require('mongodb').MongoClient;

module.exports.runDbCommandsAsync = runDbCommandsAsync;
module.exports.getClientConfigItem = getClientConfigItem;
module.exports.getClientConfig = getClientConfig;
module.exports.setClientConfigItem = setClientConfigItem;
module.exports.addRandomNick = addRandomNick;
module.exports.getKeyWords = getKeyWords;
module.exports.getGifs = getGifs;
module.exports.getBotResponses = getBotResponses;
module.exports.getRandomNick = getRandomNick;
module.exports.getWarningRoles = getWarningRoles;

//#region READ

function getRandomNick(server, callback){
    /*
    db.getCollection('nick_names').aggregate([
    { $match : { "server" : { $in : [ "global", "xxx" ] } } },
    { $sample: { size: 1 } }
    ])
    */
    var query = [
        { $match : { "server" : { $in : [ "global", server ] } } },
        { $sample: { size: 1 } }
    ];
    aggregate(query, "nick_names", function(result, err){
        if (result){
            callback(result[0]); 
        }
        else{
            callback();
        }  
    });
    
}

function getGifs(callback) {
    var query = { };
    find(query, "gif", null, function(result, err){
        if (result){
            callback(result); 
        }
        else{
            callback();
        }  
    });
};

function getBotResponses(callback) {
    var query = { };
    find(query, "bot_responses", null, function(result, err){
        if (result){
            callback(result); 
        }
        else{
            callback();
        }  
    });
};

function getKeyWords(type, callback) {
    var query = { type : type };
    find(query, "key_words", null, function(result, err){
        if (result){
            callback(result); 
        }
        else{
            callback();
        }  
    });
};

function getClientConfig(serverId, callback) {
    var query = { server: serverId };
    find(query, "config", null, function(result, err){
        //console.log(result);
        if (result){
            logger.debug('Got config items ' + JSON.stringify(result));            
            callback(result); 
        } 
        else{
            //Can have empty config
            callback(); 
        }        
    });
};

function getClientConfigItem(serverId, item, callback) {
    var query = { server: serverId, option : item };
    findOne(query, "config", null, function(result, err){
        //console.log(err);
        if (result && result != null){
            logger.debug('Got config item ' + JSON.stringify(result));
            callback(result.value, null); 
        }
        else{            
            logger.error("Could not find config item " + item + " (" + err + ")");
            callback(null, err);          
        }  
    });
};

function getWarningRoles(serverId, callback) {
    var query = { server: serverId, option : /.*warning.*/ };
    var sort = { option : 1 };
    find(query, "config", sort, function(result, err){
        //console.log(err);
        if (result && result != null){
            logger.debug('Got warning roles ' + JSON.stringify(result));
            callback(result, null); 
        }
        else{            
            logger.error("Could not find warning roles (" + err + ")");
            callback(null, err);          
        }  
    });
};

function find(query, collection, sort, callback){
    logger.debug("Finding " + JSON.stringify(query) + " on " + collection);
    MongoClient.connect(process.env.CONNECTION_STRING, { useNewUrlParser: true, useUnifiedTopology: true }, function(err, client) {
        if (err) {
            callback(null, err);
        }
        else{
            const dbo = client.db(process.env.DB_NAME);        
            dbo.collection(collection).find(query).sort(sort).toArray(function(err, result) {
                if (err) {
                    callback(null, err);
                }
                else{
                    client.close();
                    callback(result, null);
                }                
            });
        }        
    });
}

function findOne(query, collection, sort, callback){
    logger.debug("Finding (one) " + JSON.stringify(query) + " on " + collection);
    MongoClient.connect(process.env.CONNECTION_STRING, { useNewUrlParser: true, useUnifiedTopology: true, }, function(err, client) {
        if (err) {
            callback(null, err);
        }
        else{
            const dbo = client.db(process.env.DB_NAME);        
            dbo.collection(collection).findOne(query, null, function(err, result) {
                if (err) {
                    callback(null, err.toString());
                }
                else{
                    client.close();
                    callback(result, null);
                }  
            });
        }          
    });
}

function aggregate(query, collection, callback){
    logger.debug("Aggregating " + JSON.stringify(query) + " on " + collection);
    MongoClient.connect(process.env.CONNECTION_STRING, { useNewUrlParser: true, useUnifiedTopology: true }, function(err, client) {
        if (err) {
            callback(null, err);
        }
        else{
            const dbo = client.db(process.env.DB_NAME);        
            dbo.collection(collection).aggregate(query).toArray(function(err, result) {
                if (err) {
                    callback(null, err);
                }
                else{
                    client.close();
                    callback(result, null);
                }                
            });
        }        
    });
}

//#region WRITE

function addRandomNick(nick, tags, is_nsfw, serverId, callback) {
    var filter = { nick_name: nick, server : serverId };
    var update = { $set: { tags : tags, nsfw : is_nsfw  } };
    updateOne(filter, update, "nick_names", true, function(result, error){
        callback(result, error);
    });
};

function setClientConfigItem(serverId, item, configValue, callback) {
    var filter = { server: serverId, option : item };
    var update = { $set: { value : configValue } };
    updateOne(filter, update, "config", true, function(result, error){
        callback(result, error);
    });
};

function updateOne(filter, update, collection, upsert = true, callback){
    logger.debug("Creating " + JSON.stringify(update) + " in " + collection);
    MongoClient.connect(process.env.CONNECTION_STRING, { useNewUrlParser: true, useUnifiedTopology: true, }, function(err, client) {
        if (err){
            logger.error(err);
            callback(null, "Unable to connect to database : " + err.toString());
        }
        else{
            const dbo = client.db(process.env.DB_NAME);    
            dbo.collection(collection).findOneAndUpdate(filter, update, { upsert: upsert, returnNewDocument: true }, function(err, result) {
                if (err){
                    client.close();
                    logger.error(err);
                    callback(null, "Unable to update : " + err.toString());
                }
                else{
                    //console.log(result.value);
                    client.close();
                    callback(result.value, null);
                }            
            });
        }        
    });
}

async function updateBulkAsync(bulkArray, collection){
    var client = await MongoClient.connect(process.env.CONNECTION_STRING, { useNewUrlParser: true, useUnifiedTopology: true, });       
    var dbo = client.db(process.env.DB_NAME);   
    var result = await (await dbo.collection(collection).bulkWrite(bulkArray));
    return result;
}

async function runDbCommandsAsync(bulkCommands, callback){
    //logger.debug("Running " + JSON.stringify(bulkCommands));
    try{
        for (const command of bulkCommands) {
            var result = await updateBulkAsync(command.queries, command.collection);
        };
        callback();
    }
    catch(error){
        callback(error);
    }
}

//#endregion