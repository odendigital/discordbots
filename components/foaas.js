//https://foaas.com/operations
var url = 'https://foaas.com'
var logger = require('./logger.js');
var util = require('./util.js');
const https = require('https');
var operations;

exports.random = function(name, callback){
    logger.debug("---> getting random fuck");
    
    getRandomOperation(function(operation){
        logger.debug(operation);
        var url = operation.url.replace(":from", "%20").replace(":name", name);
        logger.debug(url);
        var options = {
            host: 'foaas.com',
            port: 443,
            path: url,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
              }
        };
        
        var requestData = "";
        var req = https.request(options, function(res) {   
            res.on('data', function (chunk) {
                requestData += chunk;
              });
    
            res.on('end', function() {
                var msg = JSON.parse(requestData).message;
                callback(msg);
            });
        });
        
        req.end();
    });   
}

function getRandomOperation(callback){
    if (!operations){
        logger.debug("foaas operations was empty");
        getOperations(function(opsData){
            operations =  opsData;      
            rnd = util.getRandomInt(1, operations.length);      
            callback(operations[rnd]);
        });
    }
    else{
        logger.debug("foaas operations was NOT empty");
        rnd = util.getRandomInt(1, operations.length);
        callback(operations[rnd]);
    }
}

function getOperations(callback){
    var opsData = "";
    var options = {
        host: 'foaas.com',
        port: 443,
        path: '/operations',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'accept': 'application/json'
          }
        //headers: { 'message': text }
    };
    
    var req = https.request(options, function(res) {
        //console.log("statusCode: ", res.statusCode);
        //console.log("headers: ", res.headers);    
        res.on('data', function (chunk) {
            opsData += chunk;
          });

        res.on('end', function() {
            //console.log(opsData);
            opsData = JSON.parse(opsData);
            callback(opsData);
        });
    });
    
    req.end();
}

