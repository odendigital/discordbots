var giphy = require('giphy-api')(process.env.GIPHY_KEY);
var logger = require('./logger.js');
// Require with the public beta key
//var giphy = require('giphy-api')();

//https://www.npmjs.com/package/giphy-api

exports.random = function(searchPhrase, callback){
    giphy.random(searchPhrase, function (err, res) {
        logger.debug("searching giphy for random " + searchPhrase);
        if (err){
            logger.error(err);
        }
        callback(res, err);     
    });
}
