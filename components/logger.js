const winston = require('winston');
const { format } = require('logform');
const logger = winston.createLogger({
    level: process.env.LOG_LEVEL,
    format: format.combine(
        format.errors({ stack: true }),
        winston.format.json(),
    ),
    defaultMeta: { service: 'supreme-leader' },
    // transports: [
    //     new winston.transports.File({ filename: './logs/error.log', level: './logs/error' }),
    //     new winston.transports.File({ filename: './logs/combined.log' })
    // ]
});

// Setup development
if (process.env.NODE_ENV !== 'production') {       
}

const alignedWithColorsAndTime = format.combine(
    format.colorize(),
    format.timestamp(),
    format.align(),
    format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
);
logger.add(new winston.transports.Console({
    level : 'debug',
    format: alignedWithColorsAndTime
}));

exports.debug = function(message) {
    logger.debug(message);
};

exports.info = function(message) {
    logger.info(message);
};

exports.error = function(message) {
    logger.error(message);
};

exports.warn = function(message) {
    logger.warn(message);
};