
const logger = require('./logger.js');
const discord = require('discord.js');
const data = require('./data.js');

module.exports.postMessage = postMessage;
module.exports.postReply = postReply;

function postMessage(gif, msg, client, message, channel){
    if (channel){
        postMessageToChannel(gif, msg, client, channel);
    }
    else{
        data.getClientConfigItem(message.channel.guild.id, "notice_channel", function(configRes){
            postMessageToChannel(gif, msg, client, configRes);
        });
    }
};

function postMessageToChannel(gif, msg, client, channel){
    logger.debug("Posting message to channel " + channel);
    if (gif && msg){
        var msg = new discord.RichEmbed()
        .setDescription(msg)    
        .setImage(gif)        
        client.channels.get(channel).send(msg);
    }
    else if (gif){
        client.channels.get(channel).send(gif);
    }
    else{
        client.channels.get(channel).send(msg);
    }
}

function postReply(gif, msg, message){
    if (gif && msg){
        var msg = new discord.RichEmbed()
            .setDescription(msg)    
            .setImage(gif)        
        message.reply(msg);
    }
    else if (gif){
        message.reply(gif);
    }
    else{
        message.reply(msg);
    }
}