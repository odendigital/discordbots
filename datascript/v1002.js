
module.exports.version = "1.0.0.2";
module.exports.data = getQuery;

function getQuery(){
    return [        
        { collection : "key_words", queries : [ 
            { updateOne: { filter: { server : "global", phrase: "i can play", reaction_text : "FUCK YES!", reaction_search : "fuck yes", type : "react" }, update: { $set: { type : "react" } }, upsert:true } },
            { updateOne: { filter: { server : "global", phrase: "lets play", reaction_text : "FUCK YES!", reaction_search : "fuck yes", type : "react" }, update: { $set: { type : "react" } }, upsert:true } },
            { updateOne: { filter: { server : "global", phrase: "brb", reaction_text : ":(", reaction_search : "goodbye", type : "react" }, update: { $set: { type : "react" } }, upsert:true } },
        ]},
        { collection : "bot_responses", queries : [ 
            { updateOne: { filter: { server : "global", phrase: "Sorry, I don't speak 'little bitch'", type : "dismiss" }, update: { $set: { type : "dismiss" } }, upsert:true } },
        ]},
        { collection : "config", queries : [ 
            { updateOne: { filter: { server: "global", option : "db_version" }, update: { $set: { value : "1.0.0.2" } }, upsert:true } },
        ]}
    ];
}