
module.exports.version = "1.0.0.3";
module.exports.data = getQuery;

function getQuery(){
    return [        
        { collection : "key_words", queries : [ 
            { updateOne: { filter: { server : "global", phrase: "nigga", type : "insult" }, update: { $set: { type : "insult" } }, upsert:true } },
            { updateOne: { filter: { server : "global", phrase: "nigger", type : "insult" }, update: { $set: { type : "insult" } }, upsert:true } },
            { updateOne: { filter: { server : "global", phrase: "^invite$", IsRegEx : true, reaction_text : "NOW!", reaction_search : "now", type : "react" }, update: { $set: { type : "react" } }, upsert:true } },
            { updateOne: { filter: { server : "global", phrase: "invite", IsRegEx : false, reaction_text : "NOW!", reaction_search : "now", type : "react" }, update: { $set: { type : "react" } }, upsert:true } },
            { updateOne: { filter: { server : "global", phrase: "^play$", IsRegEx : true, reaction_text : "NOW!", reaction_search : "now", type : "react" }, update: { $set: { type : "react" } }, upsert:true } },
            { updateOne: { filter: { server : "global", phrase: "start\\s{1}\\S*\\s?without me", IsRegEx : true, reaction_text : "miss you", reaction_search : "fuck yes", type : "react" }, update: { $set: { type : "react" } }, upsert:true } },
        ]},
        { collection : "config", queries : [ 
            { updateOne: { filter: { server: "global", option : "db_version" }, update: { $set: { value : "1.0.0.3" } }, upsert:true } },
        ]}
    ];
}