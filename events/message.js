const data = require('../components/data.js');
const giphy = require('../components/giphy.js');
const foaas = require('../components/foaas.js');
const logger = require('../components/logger.js');
const cache = require('../components/cache.js')
const messaging = require('../components/messaging.js')

const discord = require('discord.js');
const fs = require('fs');
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

//Dynamacally parse commands https://discordjs.guide/command-handling/#dynamically-reading-command-files
discord.commands = new discord.Collection();
for (const file of commandFiles) {
	const command = require(`../commands/${file}`);
	discord.commands.set(command.name, command);
}
    
module.exports = (client, message) => {
    if (message.author.id != client.user.id){
        parseCommand(client, message);
        // if (!checkMessageToBot(client, message)){
        //     if (message.content.substring(0, 1) === '!') {
        //         parseCommand(client, message);
        //     }
        //     else {
        //         checkTrashTalk(client, message);
        //     }
        // }
    }
}

//parse bot commands firts
function parseCommand(client, message){
    if (!message.content.startsWith("!") || message.author.bot) {
        parseMessage(client, message);
    }
    else{
        var args = message.content.slice(1).split(/ +/);
        var command = args.shift().toLowerCase();        

        if (!discord.commands.has(command)){
            logger.error("Unknown command " + command);
            return;
        }
        else{
            try {          
                args = parseArguments(args);
                logger.debug("Command : " + command + ", args :" + JSON.stringify(args));
                discord.commands.get(command).execute(client, message, args);
            } 
            catch (error) {
                //console.log(error);
                logger.error(error);
                message.reply('there was an error trying to execute that command!');
            }
        }
    }
}

function parseArguments(args){
    logger.debug("Parsing args (" + args + ")");
    if (args.length > 0){
        args = args.join(" ");
        if (args.includes("<@")){
            logger.debug("Removing user mentions from args");
            args = args.replace(/\<\@[0-9]+\>/g, '');
        }
        //console.log(args);
        var result;
        var split = args.split(":");
        if (split.length == 1){
            result = {
                param : split[0].trim()
            }
        }
        else{
            result = {};
            args.split(',').forEach(function(x){
                var arr = x.split(':');
                arr[1] && (result[arr[0].trim()] = arr[1].trim());
            });
        }
        return result;
    }
    else{
        return null;
    }
}

// Parse regular message
function parseMessage(client, message){
    checkMessageToBot(client, message);
}

//check if two users used dirty words, this will not return
function checkTrashTalk(client, message){
    logger.debug("---> Checking trash talk");
    if (message.mentions.users.size > 0) {
        logger.debug("has mentions");
        checkInsults(message, function(result){
            if (result) {
                logger.debug("swing that hammer");
                cache.getRandomGif("jerry-springer", function(gif){
                    message.reply(gif.url);
                });         
            }
            else{
                logger.debug("no insults");
                checkMessageForReactions(client, message);
            }
        });
    }
    else{
        logger.debug("no mentions");
        checkMessageForReactions(client, message);
    }    
}

function checkInsults(message, callback, loopCount = 0){
    logger.debug("---> Checking insults, loop at " + loopCount);
    cache.getInsults(function(insults){
        var found = false;
        insults.forEach(insult => {
            if (insult.isRegEx){
                console.log("*************checking reg ex " + insult);
                if (message.content.match(insult)){
                    logger.debug("insult " + insult + " found in phrase");
                    found = true;
                    return;
                }
            }
            else if (message.content.includes(insult.phrase)){
                logger.debug("insult " + insult + " found in phrase");
                found = true;
                return;
            }
        });        
        callback(found); 
    });
}

function checkMessageToBot(client, message){
    logger.debug("---> Checking bot messages");
    if (message.content.includes(client.user.toString())) {
        var user = message.author.username;
        checkInsults(message, function(result){
            if (result){
                //foaas
                foaas.random(user, function(res){
                    messaging.postReply(null, res, message);
                    //message.reply(res);
                });
            }
            else{
                cache.getRandomBotResponse("dismiss", function(response){
                    messaging.postReply(null, response.phrase, message);
                   // message.reply(response.phrase);
                });
            }
        });        
    }
    else{
        checkTrashTalk(client, message);
    }
}

function checkMessageForReactions(client, message){
    logger.debug("---> Checking for react words");
    cache.getReactWords(function(reactWords){
        var messageContent = message.content.toLowerCase();
        //console.log(keyWords);
        var reaction;
        reactWords.forEach(word => {            
            console.log(word);
            if (word.server == "global" || word.server == message.channel.guild.id){
                if (word.isRegEx){
                    if (new RegExp(word.phrase, "i").test(messageContent)){
                        logger.debug("raction matched with " + word.phrase);
                        reaction = word;  
                        return;
                    }
                }
                else if (messageContent.includes(word.phrase)){
                    logger.debug("raction matched with " + word.phrase);
                    reaction = word;                    
                    //console.log(word);                   
                    return;
                }
            }            
        });  
        if (reaction){
            console.log(reaction);
            if (reaction.reaction_url){                        
                //console.log("Positng direct url");
                messaging.postReply(reaction.reaction_url, reaction.reaction_text, message);
                //message.reply(word.reaction_url);
            }
            else{
                giphy.random(reaction.reaction_search, function(res, err){
                    console.log("Got random gif");
                    if (res){
                        messaging.postReply(res.data.image_url, reaction.reaction_text, message);
                        //message.reply(res.data.image_url);
                    }
                }); 
            }
        }
        else{
            logger.debug("no reaction found");
        }
    });
}



